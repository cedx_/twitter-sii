import { Component, OnInit, OnDestroy } from '@angular/core';
import { TwitterApiService } from './services/twitter-api.service';
import { BehaviorSubject, Subscription, Subject, Observable } from 'rxjs/Rx';
import { DEFAULTS } from './app.config';
import { ActionData } from './models/data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  private _subscriptions: Subscription[];
  private _subjects: BehaviorSubject<any[]>[];

  public accounts: any[];

  constructor(private _twitterService: TwitterApiService) {
    this._subjects = [];
    this.accounts = DEFAULTS.defaults_users;
  }

  ngOnInit() {
    // Show defaults account on init
    this.accounts.forEach((_screen_name: string) => {
      this._twitterService
        .getTimeline({
          screen_name: _screen_name,
          count: DEFAULTS.count
        })
        .subscribe((data: any[]) => {
          this._subjects.push(new BehaviorSubject<any[]>(data));
        });
    });
  }

  private doMoreTweets(eventData: ActionData) {
    this._twitterService
      .getTimeline({
        screen_name: eventData.screen_name,
        count: eventData.current_count
      })
      .subscribe(response => {
        eventData.subject.next(response);
      });
    console.log(eventData);
  }

  ngOnDestroy() {
    // Unsubscribe to all streams
    // No streams registered here at the moment
    if (this._subscriptions.length) {
      this._subscriptions.forEach(_subscription => {
        _subscription.unsubscribe();
      });
    }
  }
}
