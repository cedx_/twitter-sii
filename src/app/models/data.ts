import { BehaviorSubject } from "rxjs/Rx";

export class ActionData {
  public screen_name: string;
  public subject: BehaviorSubject<any[]>;
  public current_count: number;
}
