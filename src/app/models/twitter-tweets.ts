export class Tweets {
  public id: string;
  public created_at: string;
  public text: string;
  public tweet_link: string;

  constructor(data: any) {
    this.id = data.id;
    this.created_at = data.created_at;
    this.text = data.text;
    this.tweet_link = `https://twitter.com/statuses/${data.id_str}`;
  }
}
