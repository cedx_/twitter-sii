export class UserData {
  public id: string;
  public name: string;
  public screen_name: string;
  public location: string;
  public description: string;
  public url: string;
  public profile_image_url: string;
  public profile_banner_url: string;

  constructor(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.screen_name = data.screen_name;
    this.location = data.location;
    this.url = data.url;
    this.profile_image_url = data.profile_image_url;
    this.profile_banner_url = data.profile_banner_url;
    this.description = data.description;
  }
}
