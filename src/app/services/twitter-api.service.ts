import { Injectable } from '@angular/core';
import {
  Headers,
  Http,
  JSONPBackend,
  RequestOptionsArgs
} from '@angular/http';

import { TOKEN, API } from '../app.config';
import 'rxjs/Rx';

@Injectable()
export class TwitterApiService {
  private _requestHeaders: Headers;

  constructor(private _http: Http) {
    this._requestHeaders = new Headers();
    this._requestHeaders.append('Content-Type', 'application/json');
    this._requestHeaders.append('Authorization', TOKEN.bearer.toString());
  }

  /**
   * Get the user timeline
   *
   * @param {*} [params]
   * @returns
   * @memberof TwitterApiService
   */
  public getTimeline(params: any = {}) {
    params.exclude_replies = true;
    const options: RequestOptionsArgs = {
      headers: this._requestHeaders,
      params: params
    };

    return this._http.get(API.timeline, options).map(data => data.json());
  }
}
