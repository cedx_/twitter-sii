import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JsonpModule, HttpModule  } from '@angular/http';

import { AppComponent } from './app.component';
import { TweetListComponent } from './components/tweet-list/tweet-list.component';
import { SelectedAccountComponent } from './components/selected-account/selected-account.component';
import { SearchUsernameComponent } from './components/search-username/search-username.component';
import { TwitterApiService } from './services/twitter-api.service';
import { OAuthService } from 'angular2-oauth2';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    TweetListComponent,
    SelectedAccountComponent,
    SearchUsernameComponent,
    PaginationComponent
  ],
  imports: [BrowserModule, JsonpModule, HttpModule],
  providers: [TwitterApiService, OAuthService ],
  bootstrap: [AppComponent]
})
export class AppModule {}
