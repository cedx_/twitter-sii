import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserData } from '../../models/twitter-user';
import { Tweets } from '../../models/twitter-tweets';
import { BehaviorSubject } from 'rxjs/Rx';
import { ActionData } from '../../models/data';
import { DEFAULTS } from '../../app.config';

@Component({
  selector: 'app-tweet-list',
  templateUrl: './tweet-list.component.html',
  styleUrls: ['./tweet-list.component.less']
})
export class TweetListComponent implements OnInit {
  @Input()
  set timelineData(value: BehaviorSubject<any[]>) {
    if (!value) {
      return;
    }

    this._data = value;
    this._data.asObservable().subscribe((data: any[]) => {
      this._user = new UserData(data[0].user);
      this._tweets = data.map(_data => new Tweets(_data));
    });
    console.log('data', this._tweets);
  }

  @Output() loadMore: EventEmitter<any> = new EventEmitter<any>();

  private _data: BehaviorSubject<any[]>;
  private _tweets: any[];
  private _user: any;
  private current_count: number = DEFAULTS.count;

  constructor() {}

  ngOnInit() {}

  /**
   * Emit the current subject then fill it with api data
   *
   * @param {any} evt
   * @memberof TweetListComponent
   */
  loadMoreTweets(evt) {
    evt.preventDefault();
    this.current_count += DEFAULTS.count * 2;
    const evtResponse: ActionData = {
      screen_name: this._user.screen_name,
      current_count: this.current_count,
      subject: this._data
    };
    this.loadMore.emit(evtResponse);
  }
}
