import { Component, OnInit, Input } from '@angular/core';
import { UserData } from '../../models/twitter-user';

@Component({
  selector: 'app-selected-account',
  templateUrl: './selected-account.component.html',
  styleUrls: ['./selected-account.component.less']
})
export class SelectedAccountComponent implements OnInit {
  private _user: UserData;

  @Input()
  set user(value: UserData) {
    if (!value){
      return;
    }
    this._user = value;
  }

  constructor() {}

  ngOnInit() {}
}
