import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedAccountComponent } from './selected-account.component';

describe('SelectedAccountComponent', () => {
  let component: SelectedAccountComponent;
  let fixture: ComponentFixture<SelectedAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
