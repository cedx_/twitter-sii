export const TOKEN = {
  bearer:
    'bearer AAAAAAAAAAAAAAAAAAAAADwwFQAAAAAAKPn%2Bo5WEtfAweLzK%2BjmCiNzXqVk%3DviZjqeZZnvyqXcIM7Bjtw4Hxy39wfNmIKg9qbXJ6I1aR0m6By5'
};

/**
 * Note: I use a custom proxy server
 * to avoid CROSS problems with twitter api
 */
export const API = {
  timeline: 'https://cors-anywhere.herokuapp.com/https://api.twitter.com/1.1/statuses/user_timeline.json'
};

export const DEFAULTS = {
  count: 5,
  defaults_users: ['SIICanada', 'CanadiensMTL']
};
